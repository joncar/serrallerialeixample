<!doctype html>
<html lang="en">
	<title><?= empty($title) ? 'Monalco' : $title ?></title>
  	<meta name="keywords" content="<?= empty($keywords) ?'': $keywords ?>" />
	<meta name="description" content="<?= empty($keywords) ?'': str_replace("\"","",strip_tags($description)) ?>" /> 	
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>	
	<link rel="shortcut icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>
	<link href="<?= base_url() ?>js/stocookie/stoCookie.css" rel="stylesheet">
	<script>var URL = '<?= base_url() ?>';</script>
    <?php 
    if(!empty($css_files)):
    foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?= $file ?>" />
    <?php endforeach; ?>    
    <?php endif; ?>


    <!-- Bootstrap core CSS -->
	<link href="<?= base_url() ?>theme/theme/css/plugins/bootstrap.min.css" rel="stylesheet">
	<link href="<?= base_url() ?>theme/theme/css/plugins/bootstrap-submenu.css" rel="stylesheet">
	<link href="<?= base_url() ?>theme/theme/css/plugins/animate.min.css" rel="stylesheet">
	<link href="<?= base_url() ?>theme/theme/css/plugins/slick.css" rel="stylesheet">	
	<link href="<?= base_url() ?>theme/theme/css/plugins/magnific-popup.css" rel="stylesheet">	
	<link href="<?= base_url() ?>theme/theme/css/plugins/bootstrap-datetimepicker.css" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">    
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<!-- Icon Font-->
	<link href="<?= base_url() ?>theme/theme/iconfont/style.css" rel="stylesheet">
	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Muli:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Chau Philomene One&subset=latin" rel="stylesheet" type="text/css">		
	<!-- Google map -->
	<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyDyPEFmiS1aSSx_fpoB5US78NBVI2pmRjc&libraries=drawing,geometry,places"></script> 
	<link href="<?= base_url() ?>theme/theme/css/custom.css" rel="stylesheet">  
   
</head>

<body <?= $this->router->fetch_method()=='index'?'class="home"':'' ?>>	
	<!-- Loader -->
	<?php if($this->router->fetch_class()!='sites'): ?>
	<div id="loader-wrapper">
		<div class="loader">
			<img src="<?= base_url() ?>theme/theme/images/gif-animat.gif">			
		</div>
	</div>
	<?php endif ?>
	<!-- //Loader -->
	<?php 
		if(empty($editor)){
			$this->load->view($view); 
		}else{
			echo $view;
		}
	?>		
	<?php $this->load->view('includes/template/scripts'); ?>
	<a href="whatsapp://send?text=Hola, vull contactar amb vosaltres!&phone=+34691802758" style="font-size:18px;padding:18px 18px;text-decoration:none;background-color:#189D0E;color:white;text-shadow:none;position:fixed;bottom:0px;right:0px;z-index: 10000"> <i class="fa fa-whatsapp fa-2x"></i> Contacta'ns</a>
</body>
</html>