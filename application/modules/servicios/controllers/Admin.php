<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        public function servicios(){
            $crud = $this->crud_function('','');
            $crud->field_type('foto','image',array('path'=>'img/servicios','width'=>'370px','height'=>'310px'));
            $crud->field_type('idioma','dropdown',array('ca'=>'Catalán','es'=>'Castellano','en'=>'Ingles'));                                    
            if($crud->getParameters()=='add'){
                $crud->set_rules('titulo','Titulo','required|is_unique[servicios.titulo]');
            }
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function servicios_productos(){
            $crud = $this->crud_function('',''); 
            $crud->set_subject('Productos');
            $crud->set_relation('servicios_id','servicios','titulo'); 
            $crud->field_type('foto','image',array('path'=>'img/servicios','width'=>'580px','height'=>'322px'));
            $crud->set_field_upload('pdf','files/servicios');
            $crud->set_clone();
            $crud = $crud->render();
            $crud->title = 'Productos';
            $this->loadView($crud);
        }
        
        public function clonarEntrada($id){
            if(is_numeric($id)){
                $entry = new Bdsource();
                $entry->where('id',$id);
                $entry->init('blog',TRUE,'entrada');
                $data = $this->entrada;
                $entry->save($data,null,TRUE);
                header("Location:".base_url('blog/admin/blog/edit/'.$entry->getid()));
            }
        }
    }
?>
