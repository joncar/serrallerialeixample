<?php 
    require_once APPPATH.'controllers/Main.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();       
            $this->load->model('querys');     
        }
        
        public function read($id){                       
            if(!empty($id)){
                $blog = $this->db->get_where('servicios',array('url'=>$id));
                if($blog->num_rows()>0){
                	$blog = $blog->row();
	                $this->loadView(
	                    array(
	                        'view'=>'detail',
	                        'detail'=>$blog,
	                        'title'=>$blog->titulo,                        
	                    ));
            	}else{
	                throw new Exception('No se encuentra la entrada solicitada',404);
	            }
            }else{
                throw new Exception('No se encuentra la entrada solicitada',404);
            }
        }
        
        public function comentarios(){
            $this->load->library('form_validation');
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('autor','Autor','required')
                                  ->set_rules('texto','Comentario','required')
                                  ->set_rules('blog_id','','required|numeric');
            if($this->form_validation->run()){
                $data = array();
                foreach($_POST as $n=>$p){
                    $data[$n] = $p;
                }
                $data['fecha'] = date("Y-m-d");
                $this->db->insert('comentarios',$data);
                $_SESSION['mensaje'] = $this->success('Comentario añadido con éxito <script>document.reload();</script>');
                header("Location:".base_url('blog/frontend/read/'.$_POST['blog_id']));
            }else{
                $_SESSION['mensaje'] = $this->error('Comentario no enviado con éxito');
                header("Location:".base_url('blog/frontend/read/'.$_POST['blog_id']));                
            }
        }

        public function eventos(){
            $this->loadView(array(
                'view'=>'eventos'
            ));
        }
    }
?>
