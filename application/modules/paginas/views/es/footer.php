<!-- Footer -->
		<div class="page-footer" id="contacte">
			<div class="footer-content">
				<div class="footer-col-left">
					<div class="inside">
						<div class="footer-phone">
							<h2 class="h-phone">
								Telèfons de contacte: 
								<br/>
								<span class="number">
									<br/><i class="fa fa-mobile" style="color:white"></i> <a href="tel:+34691802758" style="text-decoration: none" class="color"> &nbsp; 691 802 758 </a>
									<br/><i class="fa fa-phone" style="color:white"></i> <a href="tel:+34691802758" style="text-decoration: none" class="color">  936 670 694</a>
								</span>
							</h2>
						</div>
						<div class="contact-info  editContent">
							<i class="icon icon-locate"></i>C/ Girona, 134 Barcelona
							<br> CP 08037
						</div>
						<div class="contact-info  editContent">
							<i class="icon icon-clock"></i>
							Dill-Div <span class="color">9:30h - 13:30h / 16:30h - 20:00h</span>
							
						</div>
						<div class="contact-info">
							<i class="icon fa fa-envelope"></i>
							<a href="mailto:serralleria@serrallerialeixample.es" style="color:white">
								serralleria@serrallerialeixample.es
							</a>
						</div>
						<div class="social-links">
							<ul>
								<li>
									<a class="icon icon-facebook-logo" href="#"></a>
								</li>
								<li>
									<a class="icon icon-twitter-logo" href="#"></a>
								</li>
								<li>
									<a class="icon icon-instagram-logo" href="#"></a>
								</li>
								<li>
									<a class="icon icon-google-plus-logo" href="#"></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="footer-col-right">
					<div id="footer-map" class="footer-map"></div>
				</div>
			</div>
			<div class="footer-bottom">
				<div class="container">
					<div class="copyright editContent">© <?= date("Y") ?> Serralleria L'Eixample, Tots els drets reservats</div>
				</div>
			</div>
		</div>
		<!-- //Footer -->