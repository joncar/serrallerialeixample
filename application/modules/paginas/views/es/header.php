<header class="page-header">
			<nav class="navbar" id="slide-nav">
				<div class="container">
					<div class="header-row">
						<div class="logo">
							<a href="<?= base_url() ?>"><img src="<?= base_url() ?>theme/theme/images/logo.png" alt="Logo"></a>
						</div>
						<div class="header-right">
							<button type="button" class="navbar-toggle"><i class="icon icon-lines-menu"></i></button>
							<div class="header-right-top">
								<div class="address">
									Dilluns-Dissabte <span class="custom-color">7:00AM - 8:00PM</span>
								</div>
								<a href="mailto:serralleria@serrallerialeixample.es" class="appointment"><i class="icon-shape icon"></i><span>DEMANAR VISITA</span></a>
							</div>
							<div class="header-right-bottom">
								<div class="header-phone"><span class="text">SERVEI URGENT PERMANENT</span><span class="phone-number"><span class="code"><a href="tel:+34691802758" class="color" style="text-decoration: none; font-family: 'Chau Philomene One', sans-serif">691 802 758</a></span></span>
								</div>
							</div>
						</div>
					</div>
					<div id="slidemenu">
						<div class="row">
							<div class="col-md-8">
								<div class="close-menu"><i class="icon-close-cross"></i></div>
								<ul class="nav navbar-nav">
									<li class="[active:main]"><a href="<?= base_url() ?>"><span>Inicio</span></a></li>
									<li class="[active:nosotros]"><a href="<?= base_url() ?>nosotros.html"><span>Nosotros</span></a></li>
									<li class="dropdown [active:servicios]">
										<a href="<?= base_url() ?>servicios.html" data-toggle="dropdown" data-submenu=""><span>Servicios</span><span class="ecaret"></span></a>
										<ul class="dropdown-menu" role="menu">
											<?php foreach($this->db->get_where('servicios',array('idioma'=>$_SESSION['lang']))->result() as $s): ?>
												<li>
													<a href="<?= base_url('servicio/'.toUrl($s->titulo)) ?>"><?= $s->titulo ?></a>
												</li>
											<?php endforeach ?>	
										</ul>
									</li>
									<li class="[active:blog]">
										<a href="<?= base_url() ?>blog"><span>Noticias</span></a>										
									</li>
									<li class="[active:galeria]"><a href="<?= base_url() ?>galeria.html"><span>Galeria</span></a>
									</li>
									<li class="[active:faq]"><a href="<?= base_url() ?>faq.html"><span>FAQ</span></a>
									</li>
									<li class="[active:contacto]"><a href="<?= base_url() ?>contacto.html"><span>Contacto</span></a>
									</li>
								</ul>
							</div>

							<div class="col-md-1 visible-xs" style="padding:0px">
								<ul class="nav navbar-nav" style="margin:0">
									<li class="dropdown">
										<a href="<?= base_url() ?>p/serveis" data-toggle="dropdown" data-submenu="">
											<span>ESP</span>
											<span class="ecaret"></span>
										</a>
										<ul class="dropdown-menu" role="menu">
											<li><a href="<?= base_url('main/traduccion/ca') ?>">CAT</a></li>
											<li><a href="<?= base_url('main/traduccion/es') ?>">ESP</a></li>
										</ul>
									</li>
								</ul>
							</div>
							
							<div class="col-md-3">
								<div class="search-container">
									<input placeholder="Buscar" type="text">
									<a class="button">
									<i class="icon icon-search"></i>
									</a>
								</div>
							</div>
							<div class="col-md-1 hidden-xs" style="padding:0px">
								<ul class="nav navbar-nav" style="margin:0">
									<li class="dropdown">
										<a href="<?= base_url() ?>p/serveis" data-toggle="dropdown" data-submenu="">
											<span>ESP</span>
											<span class="ecaret"></span>
										</a>
										<ul class="dropdown-menu" role="menu">
											<li><a href="<?= base_url('main/traduccion/ca') ?>">CAT</a></li>
											<li><a href="<?= base_url('main/traduccion/es') ?>">ESP</a></li>
										</ul>
									</li>
								</ul>
							</div>
							
						</div>
					</div>
				</div>
			</nav>
		</header>