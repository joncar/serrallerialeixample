<div>[header]</div>
<div id="pageTitle">
			<div class="container">
				<!-- Breadcrumbs Block -->
				<div class="breadcrumbs">
					<ul class="breadcrumb">
						<li><a href="[base_url]">Home</a></li>
						<li class="active">Notícies</li>
					</ul>
				</div>
				<!-- //Breadcrumbs Block -->
				<h1>Notícies <!--<span class="color">Posts</span>--></h1>
			</div>
		</div>
		<div id="pageContent">
			<div class="container">
				<div class="row">
					<div class="col-md-9 column-center">
						<div class="blog-post single">
							<div class="post-image">
								<a href="">
									<img src="[foto]" alt="">
								</a>
							</div>
							<ul class="post-meta">
								<li><i class="icon icon-clock"></i><span>[fecha]</span></li>								
							</ul>
							<h3 class="post-title">[titulo]</h3>
							<div class="post-author"> 
								<img src="<?= base_url() ?>theme/theme/images/blog/author-1.jpg" alt=""> <i>per</i> <b>[user]</b> </div>
							<div class="post-text">
								[texto]								
							</div>
							<div class="divider-sm"></div>
							<ul class="tags-list">
								[tags]
							</ul>
						</div>
						<div class="divider"></div>
					</div>
					<div class="col-md-3 column-right">
						
						<div class="side-block">
							<h3>Post Categories</h3>
							<ul class="category-list">
								[foreach:categorias]
								<li><a href="[base_url]blog?blog_categorias_id=[id]">[blog_categorias_nombre]  <span>([cantidad])</span></a></li>
								[/foreach]
							</ul>
						</div>
						<div class="side-block">
							<h3>Popular tags</h3>
							<ul class="tags-list">
								[tags]								
							</ul>
						</div>
						<div class="side-block">
							<h3>Últimes Notícies</h3>
							[foreach:relacionados]
							<div class="blog-post post-preview">
								<div class="post-image">
									<a href="[linke]">
										<img src="[fote]" alt="">
									</a>
								</div>
								<ul class="post-meta">
									<li><i class="icon icon-clock"></i><span>[feche]</span></li>									
								</ul>
								<h5 class="post-title">
									<a href="[link]">[titule]</a></h5>
								<ul class="post-meta">
									<li class="author">per <b><i>[usere]</i></b></li>
								</ul>
								<div class="post-teaser">
									<p>[texte]</p>
								</div>
							</div>
							[/foreach]
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- // Content  -->
<div>[footer]</div>