[header]
<div id="pageTitle">
			<div class="container">
				<!-- Breadcrumbs Block -->
				<div class="breadcrumbs">
					<ul class="breadcrumb">
						<li><a href="index.html">Inici</a></li>
						<li class="active">Serveis</li>
					</ul>
				</div>
				<!-- //Breadcrumbs Block -->
				<h1>Serveis</h1>
			</div>
		</div>
		<div id="pageContent">
			<!-- Services -->
			<div class="block">
				<div class="container">
					<h2 class="h-lg text-center">Què fem?</h2>
					<p class="info text-center">Tot tipus de serveis i comandes i feiens personalitzades. Pregunta'ns!</p>
					<div class="tab-content">
						<div class="row services-alt tab-pane fade in active" id="services1">
							
							<?php foreach($this->db->get_where('servicios',array('idioma'=>$_SESSION['lang']))->result() as $s): ?>
								<div class="services-block-alt col-xs-6 col-sm-6 col-md-4">
									<div class="image">
										<a href="<?= base_url('servicio/'.tourl($s->titulo)) ?>" class="image-scale-color">
											<img src="<?= base_url('img/servicios/'.$s->foto) ?>" alt="">
										</a>
									</div>
									<div class="caption">
										<h3 class="title"><?= $s->titulo ?></h3>
										<div class="text">
											<?= cortar_palabras(strip_tags($s->descripcion),20) ?>
										</div>
									</div>
								</div>
							<?php endforeach ?>

						</div>
					<div class="divider-lg"></div>
					<div class="text-center">
						<h2 class="h-lg">Urgències <span class="color">24h</span></h2>
						<p class="info">Truca'ns i solucionarem el teu problema!</p>
						<h2 class="h-phone">691 802 758</h2>
						<div class="btn-inline"><a class="btn btn-invert" href="#"><span>Necessites ajuda?</span></a></div>
					</div>
				</div>
			</div>
			<!-- //Services -->
			<!-- Services List Block -->
			<div class="block bg-1">
				<div class="container">
					<h2 class="h-lg text-center">Els nostres <span class="color">Serveis</span></h2>
					<p class="info text-center">Aquí tens un llistat d'alguns dels nostres serveis:</p>
					<div class="row" id="slideMobile">
						<div class="col-sm-4 col-md-4">
							<ul class="marker-list">
								<li>Duplicat de claus</li>
								<li>Duplicat i codificació de comandaments</li>
								<li>Manteniment de comunitats</li>
								<li>Servei 24h d'emergències</li>
								<li>Canvi i reparació de bombins</li>
								<li>Instal·lació de portes d'alta seguretat</li>
								<li>Instal·lació de reixes i persianes</li>
								<li>Instal·lació i reparació de panys d'alta seguretat</li>
							</ul>
						</div>
						<div class="col-sm-4 col-md-4 view-more-mobile">
							<ul class="marker-list">
								<li>Duplicat de claus</li>
								<li>Duplicat i codificació de comandaments</li>
								<li>Manteniment de comunitats</li>
								<li>Servei 24h d'emergències</li>
								<li>Canvi i reparació de bombins</li>
								<li>Instal·lació de portes d'alta seguretat</li>
								<li>Instal·lació de reixes i persianes</li>
								<li>Instal·lació i reparació de panys d'alta seguretat</li>
							
								</li>
							</ul>
						</div>
						<div class="col-sm-4 col-md-4 view-more-mobile">
							<ul class="marker-list">
								<li>Duplicat de claus</li>
								<li>Duplicat i codificació de comandaments</li>
								<li>Manteniment de comunitats</li>
								<li>Servei 24h d'emergències</li>
								<li>Canvi i reparació de bombins</li>
								<li>Instal·lació de portes d'alta seguretat</li>
								<li>Instal·lació de reixes i persianes</li>
								<li>Instal·lació i reparació de panys d'alta seguretat</li>
							
							</ul>
						</div>
					</div>
					<div class="text-center"><a href="#slideMobile" class="view-more-link color"><span class="more">All Services</span><span class="less">Hide All Services</span></a>
					</div>
				</div>
			</div>
			<!-- //Services List Block -->
			<!-- Block -->
			<div class="block">
				<div class="container">
					<div class="text-center">
						<h2 class="h-lg">Vols <span class="color">contactar amb nosaltres?</h2>
						<p class="info">Truca'ns o demana'ns el pressupost del que busques</p>
						<div class="btn-inline"><a class="btn btn-invert" href="#"><span>Demanar cita</span></a><a class="btn" href="contact.html"><span>Demanar pressupost</span></a></div>
					</div>
				</div>
			</div>
			<!-- // Block -->
		</div>
		[footer]