[header]
	<!-- Content  -->
	<div id="pageTitle">
		<div class="container">
			<!-- Breadcrumbs Block -->
			<div class="breadcrumbs">
				<ul class="breadcrumb">
					<li><a href="index.html">Inici</a></li>
					<li class="active"><?= $detail->titulo ?></li>
				</ul>
			</div>
			<!-- //Breadcrumbs Block -->
			<h1><?= $detail->titulo ?></h1>
		</div>
	</div>
	<div id="pageContent">
		<div class="block">
			<div class="container">
				<div class="row">
					<div class="col-md-5">
						<img src="<?= base_url('img/servicios/'.$detail->foto) ?>" alt="" class="img-responsive" style="width:100%;">
					</div>
					<div class="divider visible-sm visible-xs"></div>
					<div class="col-md-7">
						<?= $detail->descripcion ?>
					</div>
				</div>



				<div class="row row-fluid meet-area-txt about-us-teammember">
					<div class="wpb_column vc_column_container vc_col-sm-12">
						<div class="vc_column-inner">
							<div class="wpb_wrapper">
								<div class="wpb_text_column wpb_content_element ">
									<div class="wpb_wrapper">
										<h2 class="h-lg text-center">Els nostres productes</h2>										
									</div>
								</div>
								<div class="row">            
									
									<?php foreach($this->db->get_where('servicios_productos',array('servicios_id'=>$detail->id))->result() as $p): ?>
										<div class="col-sm-12 col-xs-12">
											<div class="person person-hor">
												<div class="image image-scale-color">
													<img src="<?= base_url('img/servicios/'.$p->foto) ?>" class="attachment-full size-full" alt="">
													<div class="hover"></div>
												</div>
												<div class="person-info">
													<?= $p->descripcion ?>
													<?php if(!empty($p->pdf)): ?>
														<div class="link">
															<a class="icon icon-check" href="<?= base_url('files/servicios/'.$p->pdf) ?>" target="_new">																
																
															</a> 
															<br/>
															<span style="vertical-align:middle;">PDF</span>
														</div>
													<?php endif ?>
												</div>
											</div>
										</div>
									<?php endforeach ?>



								</div>
							</div>
						</div>
					</div>			
				</div>


			</div>
		</div>




		






		<!-- Appointment Block -->
		<div class="block">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="text-appointment">
							<h2 class="h-lg">Vols <span class="color">contactar amb nosaltres?</h2>
							<p class="info">Truca'ns o demana'ns el pressupost del que busques</p>
							<div class="btn-inline"><a class="btn btn-invert" href="#"><span>Demanar cita</span></a><a class="btn" href="contact.html"><span>Demanar pressupost</span></a></div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="img-move animation" data-animation="fadeInRight" data-animation-delay="0s">
							<img src="<?= base_url() ?>theme/theme/images/img-car-move.png" alt="" style="width:100%">
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- //Appointment Block -->
		


	</div>
	<!-- // Content  -->
[footer]