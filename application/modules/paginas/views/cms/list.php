<div class="panel panel-default">
    <div class="panel-heading">
        <h1 class="panel-title">Lista de archivos</h1>
    </div>
    <div class="panel-body">        
        <a href="<?= base_url('paginas/admin/paginas/add') ?>" class="btn btn-success"><i class="fa fa-plus-square"></i> Añadir</a>
        <a href="<?= base_url('paginas/admin/ftp') ?>" class="btn btn-success"><i class="fa fa-file"></i> FTP</a>
        <a href="<?= base_url('paginas/sites/') ?>" class="btn btn-success"><i class="fa fa-file"></i> Editor</a>
        <a href="<?= base_url('paginas/admin/paginas/traductor') ?>" class="btn btn-success"><i class="fa fa-file"></i> Traductor</a>
        <select id="idiomas" style="float:right">
            <?php foreach(explode(',',$this->db->get('ajustes')->row()->idiomas) as $i): ?>
                <option value="<?= trim($i) ?>" <?= ($folder==trim($i))?'selected':'' ?>><?= trim($i) ?></option>
            <?php endforeach ?>
        </select>
        <?php if(!is_dir('application/modules/paginas/views/'.$folder)): ?>
            <div class="alert alert-danger">No se ha creado la carpeta contenedora de idioma</div>
        <?php endif ?>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Archivo</th><th>File</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($files as $f): ?>
                    <?php if(strstr($f,'.php') && $f!=='theme.php' && $f!=='read.php'): ?>
                        <tr>
                            <td><?= $f ?></td>
                            <td>
                                <a title="Mostrar" target="_new" href="<?= base_url('p/'.str_replace('.php','',$f)) ?>" style="color:black"><i class="fa fa-eye"></i></a>
                                <a title="Editar" href="<?= base_url('paginas/frontend/editor/'.str_replace('.php','',$f)) ?>" style="color:black"><i class="fa fa-edit"></i></a>
                                <a title="Eliminar" href="javascript:eliminar('<?= $f ?>')" style="color:red"><i class="fa fa-remove"></i></a>
                            </td>
                        </tr>
                    <?php endif ?>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
</div>
<script>
    function eliminar(file){
        if(confirm('Seguro que desea eliminar esta vista?. Esta acción no tiene vuelta atras')){
            document.location.href="<?= base_url('paginas/admin/paginas/delete/') ?>/"+file;
        }
    }

    $("#idiomas").on('change',function(){
        document.location.href="<?= base_url('paginas/admin/paginas/lang/') ?>/"+$(this).val();
    });
</script>