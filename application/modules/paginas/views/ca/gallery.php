<div>[header]</div>
<!-- Content  -->
		<div id="pageTitle">
			<div class="container">
				<!-- Breadcrumbs Block -->
				<div class="breadcrumbs">
					<ul class="breadcrumb">
						<li><a href="index.html">Inici</a></li>
						<li class="active">Galeria</li>
					</ul>
				</div>
				<!-- //Breadcrumbs Block -->
				<h1>Galeria</h1>
			</div>
		</div>
		<div id="pageContent">
			<!-- Block -->
			<div class="block">
				<div class="container">
					<!-- Filters -->
					<div class="filters-by-category">
						<ul class="option-set" data-option-key="filter">
							<li><a href="#filter" data-option-value="*" class="selected">Totes</a></li>
							<?php foreach($this->db->get('galeria')->result() as $g): ?>
								<li><a href="#filter" data-option-value=".<?= $g->id ?>cat"><?= $g->nombre ?></a></li>
							<?php endforeach ?>
						</ul>
					</div>
					<!-- //end Filters -->
					<div class="gallery gallery-isotope" id="gallery">
					<?php foreach($this->db->get_where('galeria_fotos')->result() as $g): ?>
						
							<div class="gallery-item <?= $g->galeria_id ?>cat">
								<div class="gallery-item-image">
									<img src="<?= base_url() ?>img/galeria/<?= $g->foto ?>" alt="" />
									<a class="hover" href="<?= base_url() ?>img/galeria/<?= $g->foto ?>">
									<!--<span class="view">
									<span class="icon icon-search"></span>
									</span>
									<span class="tags">
									<span class="pull-left">Oil Change</span>
									<span class="pull-right"><i class="icon icon-favorite"></i>21</span>
									</span>-->
									</a>
								</div>
							</div>
						
					<?php endforeach ?>
					</div>
				</div>
			</div>
			<!-- //Block -->
		</div>
		<!-- // Content  -->
<div>[footer]</div>