

    <div>[header]</div>
<!-- Content  -->
	<div id="pageTitle">
		<div class="container">
			<!-- Breadcrumbs Block -->
			<div class="breadcrumbs">
				<ul class="breadcrumb">
					<li><a href="index.html">Inici</a></li>
					<li class="active">Mètode Bumping</li>
				</ul>
			</div>
			<!-- //Breadcrumbs Block -->
			<h1 style="outline: currentcolor none medium; outline-offset: -2px; cursor: inherit; content: none; color: rgb(255, 255, 255); font-size: 44px; background-color: rgba(0, 0, 0, 0); font-family: &quot;Chau Philomene One&quot;, sans-serif;" class="">Mètode Bumping<span class="color"></span></h1>
		</div>
	</div>
<div id="pageContent">
		<div class="block">
			<div class="container">
				<div class="row">
					<div class="col-md-5">
						<img src="<?= base_url() ?>theme/theme/images/uploads/1/bum1.jpg" alt="" class="img-responsive" style="outline: currentcolor none medium; outline-offset: -2px; cursor: inherit;" data-background="<?= base_url() ?>theme/theme/images/uploads/1/experience-1.png">
					<br><img src="<?= base_url() ?>theme/theme/images/uploads/1/bum2.jpg" alt="" class="img-responsive" style="outline: currentcolor none medium; outline-offset: -2px; cursor: inherit;" data-background="<?= base_url() ?>theme/theme/images/uploads/1/experience-1.png">
						<br><img src="<?= base_url() ?>theme/theme/images/uploads/1/bum4.jpg" alt="" class="img-responsive" style="outline: currentcolor none medium; outline-offset: -2px; cursor: inherit;" data-background="<?= base_url() ?>theme/theme/images/uploads/1/experience-1.png">
					
					</div>
					<div class="divider visible-sm visible-xs"></div>
					<div class="col-md-7">
						<h2>Què és?</h2>
						<p>El mètode Bumping és una forma senzilla d'obertura, emprat per a casos d'extraviaments de claus, una tècnica a l'abast de qualsevol persona. Amb habilitat i un tornavís l'obertura no es resisteix, ja que amb aquest sumat a un cop s'aconsegueix obrir-la.
<h2>Una mica d'història per a saber d'on ve</h2>
A la dècada de 1970, manyans a Dinamarca compartien una tècnica per a desbloquejar els cilindres d'un pany, la tècnica consistia a pressionar lleugerament la clau amb un objecte, fent saltar els cilindres, provocant que el pany pogués lliscar-se lliurement, permetent així obrir el pany.
<br>L'obertura de cops o bumping és l'art d'obrir un pany amb una clau modi cada, comunament dita Clau de percussió. Una clau corresponent i preparada per al mètode bumping adequadament manejada per algú amb experiència en obertura de portes i panys pot obrir una porta en menys de 25 segons.
<br>L'obertura per bumping és una tècnica popular usada pels delinqüents per a obrir els panys de cilindre així com forrellats de seguretat mitjançant l'ús d'una clau especialment dissenyada anomenada clau de cop o 999 / clau de rap. Aquesta tècnica és immensament poderosa i permet que s'obri una gran varietat de panys de manera ràpida sense trencament ni soroll.
<br>Aquest post explica què és el mètode bumping, i com es pot evitar que els delinqüents puguin aplicar aquesta tècnica en els nostres panys i obrint fàcilment l'accés a la nostra propietat.<br>
<br><h2>És important tenir en compte que:</h2>
<p>L'ús d'aquesta tècnica per a un ús il·lícit, com a forma de robatori, no es va presentar fins a algun temps després, i es va
reconèixer per primera vegada com un problema de seguretat entorn dels anys 2002-2003.
<br>El bumping ha guanyat popularitat en els últims anys a causa de la simplicitat i l'e cacia d'aquesta tècnica. També a causa dels videotutoriales en Youtube, així com les botigues online on les claus de bump impreses en 3D estan disponibles a partir de 3€, això ha fet que la tècnica de bumping hagi crescut vertiginosament estant àmpliament disponible i accessible a qualsevol persona que dese aprendre a usar-la.
<br>Segons ens informen nombrosos manyans als qui hem consultat si el pany tradicional és apta per a ser oberta per la tècnica de bumping, els lladres poden obrir el 90% dels panys en menys de 25 segons aproximadament. Més alarmant és que moltes dels nous panys de mitjana qualitat són fins i tot més propenses a ser obertes amb bumping en comparació als antics panys.

<br>En l'actualitat, i per descomptat al marge de l'ús professional manyà, es ve detectant que aquesta tècnica, que permet l'accés de manera silenciosa sense deixar rastre de força en els cilindres, està sent utilitzada per a nes delictius per bandes organitzades, moltes d'elles amb preparació militar, fent ús d'aquesta tècnica en objectius especialment seleccionats (habitatges unifamiliars de persones que entenien eren objectiu per als seus interessos). Fonts policials van indicar que són les bandes de l'Est de l'Europa les que més estan utilitzant aquests mètodes.
<br>En el cas del «bumping», les bandes delinqüents de georgians i romanesos són els millors especialistes: també hi ha controlats grups de bandes organitzades de serbocroates i albanesos; i, en menor mesura, bandes de lladres d'origen sud-americà i espanyol.
<br>Gairebé qualsevol delinqüent pot accedir amb enorme facilitat a gairebé qualsevol pany mecànic, incloent les de portes de seguretat i cuirassades, sense la necessitat de conèixer tècniques de ganzuado. S'estima que aproximadament el 80% dels panys mecànics estan en aquest moment sofrint perill d'accés indesitjat.
</p>

</p>
<h2>6 dades sobre el bumping que coneix tot manyà</h2>
						<ul class="marker-list-sm">
							<li>Amb una clau de bumping és possible obrir la majoria dels panys en menys de 25 segons. </li>
							<li>Més del 90% de les portes a Espanya que usen un vell tipus de pany de cilindre que pot ser vulnerable a la tècnica bumping. </li>
							<li>Més del 95% dels nous panys de les ferreteries no especialitzades són vulnerables al bumping.</li>
							<li>La tècnica del bumping és tan fàcil que fins i tot un nen de 14 anys pot realitzar-la sense esforç. </li>
							<li>El bumping no és destructiu: no deixa cap senyal d'entrada forçada o danys quan s'utilitza una clau de cop bump. </li>
							<li>Una clau de cop per a bumping pot ser feta per qualsevol persona en menys de 5 minuts.</li>
					
						</ul>
						<h2>Com podem prevenir el bloqueig de cop o mètode bumping?</h2>
						<p>Hem d'instal·lar panys d'alta seguretat amb sistema antibumping especificat. La nostra recomanació és sempre que instal·lis un bombí de marques líders en seguretat, com Kaba O Abus, ja que per poca diferència econòmica tindràs la seguretat garantida per molts anys contra el bumping.
<br>T'aconsellem no estalviar aquesta petita diferència econòmica i decantar-te per una marca de prestigi, sobretot tenint present que és una inversió en seguretat a llarg termini, de manera que si divideixes aquesta inversió entre la vida útil d'un pany o bé tub de primera marca pels 15/20 anys de durada, veuràs que la inversió a l'any és mínima.
<br>I és important que coneguem que, nombrosos panys estan sortint al mercat per a evitar l'obertura mitjançant bumping a pesar que passaran molts anys abans que vegem que el 100% dels panys siguin capaços d'evitar ser oberts pel bumping amb tanta facilitat.

					</div>
				</div>
			</div>
		</div>
		<!-- Appointment Block -->
		<div class="block">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="text-appointment">
							<h2 class="h-lg">El millor cilindre <span class="color"> antibumping</span></h2>
							<p class="info" style="">Dormakaba s'ha convertit en un dels fabricants per excel·lència quant a cilindres d'alta seguretat a tot el món, la prova d'això és que és el cilindre més utilitzat en les principals entitats de banca a Espanya. Des de Serralleria l’Eixample us presentem el que és el nostre cilindre de referència per les altes prestacions de seguretat que ofereix, a clients particulars i a empreses. Sens dubte amb aquest cilindre vostè i la seva família descansessin tranquils. Enfront de qualsevol intent de robatori.</p>
							
							
						</div>
					</div>
					<div class="col-md-6">
						<div class="img-move animation animated fadeInRight" data-animation="fadeInRight" data-animation-delay="0s" style="animation-delay: 0s;">
							<img src="http://serrallerialeixample.es/new/theme/theme/images/Cilindro.jpg" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- //Appointment Block -->
		<!-- How It Works -->
		<div class="block bg-2" data-bg="http://serrallerialeixample.es/new/theme/theme/images/block-bg-2.jpg">
			<div class="container">
				<h2 class="h-lg text-center">Els nostres <span class="color">punts forts</span></h2>
				<div class="row how-works-row">
					<div class="col-sm-6 col-md-3">
						<div class="how-works-block">
							<div class="image">
								<div class="image-scale"><img src="http://serrallerialeixample.es/new/theme/theme/images/how-works-img-1.jpg" alt="" style=""></div>
							</div>
							<div class="caption">
								<div class="inside">
									<div class="number"><span>1</span></div>
									<div class="text">
										<h5 style="">Màxima rapidesa</h5>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-3">
						<div class="how-works-block">
							<div class="image">
								<div class="image-scale"><img src="http://serrallerialeixample.es/new/theme/theme/images/how-works-img-2.jpg" alt="" style=""></div>
							</div>
							<div class="caption">
								<div class="inside">
									<div class="number"><span>2</span></div>
									<div class="text">
										<h5 style="">Màxima qualitat</h5>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-3">
						<div class="how-works-block">
							<div class="image">
								<div class="image-scale"><img src="http://serrallerialeixample.es/new/theme/theme/images/how-works-img-3.jpg" alt="" style=""></div>
							</div>
							<div class="caption">
								<div class="inside">
									<div class="number"><span>3</span></div>
									<div class="text">
										<h5 style="">Totes les solucions</h5>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-3">
						<div class="how-works-block">
							<div class="image">
								<div class="image-scale"><img src="http://serrallerialeixample.es/new/theme/theme/images/how-works-img-4.jpg" alt="" style=""></div>
							</div>
							<div class="caption">
								<div class="inside">
									<div class="number"><span>4</span></div>
									<div class="text">
										<h5 style="">Clients satisfets</h5>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- //How It Works -->
		<!-- Statistics Block -->
		<div class="block">
			<div class="container">
				<h2 class="h-lg text-center" style="">Algunes xifres <span class="color">sobre nosaltres</span></h2>
				<p class="info text-center" style="">Procurem sempre aconseguir els millors resultats i satisfer als nostres clients</p>
				<div class="row" id="counterBlock">
					<div class="col-sm-6 col-md-3">
						<div class="stat-box">
							<div><span class="number"><span class="count" data-to="10" data-speed="1000">22</span></span><span class="icon"><i class="icon-rocket"></i></span></div>
							<div class="text">
								<h5 style="">Anys d'experiència</h5></div>
						</div>
					</div>
					<div class="col-sm-6 col-md-3">
						<div class="stat-box">
							<div><span class="number"><span class="count" data-to="32" data-speed="1000">15</span></span><span class="icon"><i class="icon-people-1"></i></span></div>
							<div class="text">
								<h5 style="">Professionals en serralleria</h5></div>
						</div>
					</div>
					<div class="col-sm-6 col-md-3">
						<div class="stat-box">
							<div><span class="number"><span class="count" data-to="2500" data-speed="1000">570</span></span><span class="icon"><i class="icon-people"></i></span></div>
							<div class="text">
								<h5 style="">clients satisfets</h5></div>
						</div>
					</div>
					<div class="col-sm-6 col-md-3">
						<div class="stat-box">
							<div><span class="number"><span class="count" data-to="1900" data-speed="1000">800</span></span><span class="icon"><i class="icon-transport"></i></span></div>
							<div class="text">
								<h5 style="">Panys reparats</h5></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	<div>[footer]</div>