<header class="page-header">
	<nav class="navbar" id="slide-nav">
		<div class="container">
			<div class="header-row">
				<div class="logo">
					<a href="<?= base_url() ?>"><img src="<?= base_url() ?>theme/theme/images/logo.png" alt="Logo"></a>
				</div>
				<div class="header-right">
					<button type="button" class="navbar-toggle"><i class="icon icon-lines-menu"></i></button>
					<div class="header-right-top">
						<div class="address">
							Dill-Div
							<span class="custom-color">
								9:30h - 13:30h / 16:30h - 20:00h
							</span>
							<br>
							
						</div>
						<a href="mailto:serralleria@serrallerialeixample.es" class="appointment"><i class="icon-shape icon"></i><span>DEMANAR VISITA</span></a>
					</div>
					<div class="header-right-bottom">
						<div class="header-phone"><span class="text">SERVEI URGENT PERMANENT</span><span class="phone-number"><span class="code"><a href="tel:+34936670694" class="color" style="text-decoration: none; font-family: 'Chau Philomene One', sans-serif">936 670 694</a></span></span>
					</div>
				</div>
			</div>
		</div>
		<div id="slidemenu">
			<div class="row">
				<div class="col-md-8">
					<div class="close-menu"><i class="icon-close-cross"></i></div>
					<ul class="nav navbar-nav">
						<!-- <li class="[active:main]"><a href="<?= base_url() ?>"><span>Inici</span></a></li> -->
						<li class="[active:nosaltres]"><a href="<?= base_url() ?>nosaltres.html"><span>Nosaltres</span></a></li>
						<li class="dropdown [active:serveis]">
							<a href="<?= base_url() ?>p/serveis" data-toggle="dropdown" data-submenu=""><span>Serveis</span><span class="ecaret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<?php foreach($this->db->get_where('servicios',array('idioma'=>$_SESSION['lang']))->result() as $s): ?>
									<li>
										<a href="<?= base_url('servei/'.toUrl($s->url)) ?>"><?= $s->titulo ?></a>
									</li>
								<?php endforeach ?>								
							</ul>
						</li>
						<li class="[active:blog]">
							<a href="<?= base_url() ?>blog"><span>Blog</span></a>
						</li>
						<li class="[active:gallery]"><a href="<?= base_url() ?>gallery.html"><span>Galeria</span></a>
					</li>
					</li>
						<li class="[active:gallery]"><a href="<?= base_url() ?>bumping.html"><span>Bumping</span></a>
					</li>
					<li class="[active:faq]"><a href="<?= base_url() ?>faq.html"><span>FAQ</span></a>
				</li>
				<li class="[active:contacte]"><a href="<?= base_url() ?>contacte.html"><span>Contacte</span></a>
			</li>
		</ul>
	</div>
	<div class="col-md-1 visible-xs" style="padding:0px">
		<ul class="nav navbar-nav" style="margin:0">
			<li class="dropdown">
				<a href="<?= base_url() ?>p/serveis" data-toggle="dropdown" data-submenu="">
					<span>CAT</span>
					<span class="ecaret"></span>
				</a>
				<ul class="dropdown-menu" role="menu">
					<li><a href="<?= base_url('main/traduccion/ca') ?>">CAT</a></li>
					<li><a href="<?= base_url('main/traduccion/es') ?>">ESP</a></li>
				</ul>
			</li>
		</ul>
	</div>
	
	<div class="col-md-3">
		<div class="search-container">
			<form action="[base_url]paginas/frontend/buscar">
				<input placeholder="Buscar" type="text" name="q">
				<button class="button" type="submit" style="border:0">
				<i class="icon icon-search"></i>
				</button>
			</form>
		</div>
	</div>
	<div class="col-md-1 hidden-xs" style="padding:0px">
		<ul class="nav navbar-nav" style="margin:0">
			<li class="dropdown">
				<a href="<?= base_url() ?>p/serveis" data-toggle="dropdown" data-submenu="">
					<span>CAT</span>
					<span class="ecaret"></span>
				</a>
				<ul class="dropdown-menu" role="menu">
					<li><a href="<?= base_url('main/traduccion/ca') ?>">CAT</a></li>
					<li><a href="<?= base_url('main/traduccion/es') ?>">ESP</a></li>
				</ul>
			</li>
		</ul>
	</div>
	
</div>
</div>
</div>
</nav>
</header>