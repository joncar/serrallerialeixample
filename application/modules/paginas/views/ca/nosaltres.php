

    <div>[header]</div>
<!-- Content  -->
	<div id="pageTitle">
		<div class="container">
			<!-- Breadcrumbs Block -->
			<div class="breadcrumbs">
				<ul class="breadcrumb">
					<li><a href="index.html">Inici</a></li>
					<li class="active">Nosaltres</li>
				</ul>
			</div>
			<!-- //Breadcrumbs Block -->
			<h1 style="outline: currentcolor none medium; outline-offset: -2px; cursor: inherit; content: none; color: rgb(255, 255, 255); font-size: 44px; background-color: rgba(0, 0, 0, 0); font-family: &quot;Chau Philomene One&quot;, sans-serif;" class="">Nosaltres<span class="color"></span></h1>
		</div>
	</div>
<div id="pageContent">
		<div class="block">
			<div class="container">
				<div class="row">
					<div class="col-md-5">
						<img src="<?= base_url() ?>theme/theme/images/uploads/1/experience-1.jpg" alt="" class="img-responsive" style="outline: currentcolor none medium; outline-offset: -2px; cursor: inherit;" data-background="<?= base_url() ?>theme/theme/images/uploads/1/experience-1.png">
					</div>
					<div class="divider visible-sm visible-xs"></div>
					<div class="col-md-7">
						<h2>Tot en serralleria</h2>
						<p>Serralleria L’Eixample, una empresa familiar amb una llarga trajectoria en el sector de la serralleria i amb més de 15 anys d’experiència. Som especialistes en sistemes de seguretat, panys, tancaments, persianes i obertura de portes. 
<br><br>Estem situats al Carrer Girona 134 de Barcelona (entre Av. Diagonal i C/Mallorca), a la nostra botiga podrà trobar-hi tot el que necessita relacionat amb la seguretat i el tancament de la porta, reixa o finestres del seu domicili o negoci.
<br><br>Oferim els nostres serveis tant a particulars com a empreses i sempre que ens demanin un servei li farem un pressupost sense cap tipus de compromís!
<br><br>Treballem per vosté i per els de casa seva per oferir-li els millors serveis i solucions en:

</p>
						<ul class="marker-list-sm">
							<li>Obertura de portes, vehicles i caixes fortes</li>
							<li>Manteniment de comunitats</li>
							<li>Panys electrònics</li>
							<li>Instal·lació, substitució i reparació de tot tipus de panys</li>
							<li>Instal·lació i reparació de persianes</li>
							<li>Duplicat de tot tipus de claus i comandaments</li>
							<li>Venda de caixes fortes Ollé</li>
							<li>Reparació i canvis de combinació de caixes fortes</li>
							<li>Instal·lació i manteniment d’alarmes TYCO</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- Appointment Block -->
		<div class="block">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="text-appointment">
							<h2 class="h-lg">Instal·lacions <span class="color"> a comunitats</span></h2>
							<p class="info" style="">Et donem solucionas per a la instal·lació en seguretat<br> de vivendes i blocs, manteniment i reparació</p>
							<h2 class="h-phone" style="">691 802 758</h2>
							<div class="btn-inline"><a class="btn btn-invert" href="#" style=""><span>Demanar cita</span></a><a class="btn" href="contact.html"><span>Sol·licitar pressupost</span></a></div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="img-move animation animated fadeInRight" data-animation="fadeInRight" data-animation-delay="0s" style="animation-delay: 0s;">
							<img src="http://serrallerialeixample.es/new/theme/theme/images/img-car-move.jpg" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- //Appointment Block -->
		<!-- How It Works -->
		<div class="block bg-2" data-bg="http://serrallerialeixample.es/new/theme/theme/images/block-bg-2.jpg">
			<div class="container">
				<h2 class="h-lg text-center">Els nostres <span class="color">punts forts</span></h2>
				<div class="row how-works-row">
					<div class="col-sm-6 col-md-3">
						<div class="how-works-block">
							<div class="image">
								<div class="image-scale"><img src="http://serrallerialeixample.es/new/theme/theme/images/how-works-img-1.jpg" alt="" style=""></div>
							</div>
							<div class="caption">
								<div class="inside">
									<div class="number"><span>1</span></div>
									<div class="text">
										<h5 style="">Màxima rapidesa</h5>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-3">
						<div class="how-works-block">
							<div class="image">
								<div class="image-scale"><img src="http://serrallerialeixample.es/new/theme/theme/images/how-works-img-2.jpg" alt="" style=""></div>
							</div>
							<div class="caption">
								<div class="inside">
									<div class="number"><span>2</span></div>
									<div class="text">
										<h5 style="">Màxima qualitat</h5>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-3">
						<div class="how-works-block">
							<div class="image">
								<div class="image-scale"><img src="http://serrallerialeixample.es/new/theme/theme/images/how-works-img-3.jpg" alt="" style=""></div>
							</div>
							<div class="caption">
								<div class="inside">
									<div class="number"><span>3</span></div>
									<div class="text">
										<h5 style="">Totes les solucions</h5>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-3">
						<div class="how-works-block">
							<div class="image">
								<div class="image-scale"><img src="http://serrallerialeixample.es/new/theme/theme/images/how-works-img-4.jpg" alt="" style=""></div>
							</div>
							<div class="caption">
								<div class="inside">
									<div class="number"><span>4</span></div>
									<div class="text">
										<h5 style="">Clients satisfets</h5>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- //How It Works -->
		<!-- Statistics Block -->
		<div class="block">
			<div class="container">
				<h2 class="h-lg text-center" style="">Algunes xifres <span class="color">sobre nosaltres</span></h2>
				<p class="info text-center" style="">Procurem sempre aconseguir els millors resultats i satisfer als nostres clients</p>
				<div class="row" id="counterBlock">
					<div class="col-sm-6 col-md-3">
						<div class="stat-box">
							<div><span class="number"><span class="count" data-to="10" data-speed="1000">22</span></span><span class="icon"><i class="icon-rocket"></i></span></div>
							<div class="text">
								<h5 style="">Anys d'experiència</h5></div>
						</div>
					</div>
					<div class="col-sm-6 col-md-3">
						<div class="stat-box">
							<div><span class="number"><span class="count" data-to="32" data-speed="1000">15</span></span><span class="icon"><i class="icon-people-1"></i></span></div>
							<div class="text">
								<h5 style="">Professionals en serralleria</h5></div>
						</div>
					</div>
					<div class="col-sm-6 col-md-3">
						<div class="stat-box">
							<div><span class="number"><span class="count" data-to="2500" data-speed="1000">570</span></span><span class="icon"><i class="icon-people"></i></span></div>
							<div class="text">
								<h5 style="">clients satisfets</h5></div>
						</div>
					</div>
					<div class="col-sm-6 col-md-3">
						<div class="stat-box">
							<div><span class="number"><span class="count" data-to="1900" data-speed="1000">800</span></span><span class="icon"><i class="icon-transport"></i></span></div>
							<div class="text">
								<h5 style="">Panys reparats</h5></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- //Statistics Block -->
		<!-- Team Block -->
		<div class="block">
			<div class="container">
				<h2 class="h-lg text-center" style="">El nostre <span class="color">equip</span></h2>
				<p class="info text-center" style="">Som un equip de tècnics professionals, especialitzats en diferents tipus de tancaments i protecció de tancaments</p>
				<div class="row person-carousel slick-initialized slick-slider">
					<div aria-live="polite" class="slick-list draggable" tabindex="0"><div class="slick-track" style="opacity: 1; width: 1200px; transform: translate3d(0px, 0px, 0px);"><div class="col-sm-6 col-md-3 slick-slide slick-active" data-slick-index="0" aria-hidden="false" style="width: 300px;">
						<div class="person">
							<div class="image image-scale-color"><img src="http://serrallerialeixample.es/new/theme/theme/images/person-02.jpg" alt="" class="img-responsive" style="">
								<div class="hover"></div>
							</div>
							<h5 class="name" style="">Robert J. Piedra</h5>
							<h6 class="position" style="">Gerent</h6>
							<div class="text">El nostre objectiu és satisfer els nostres clients amb una qualitat suprema.</div>
							<div class="link">
								<a class="icon icon-facebook-logo" href="#"></a>
								<a class="icon icon-twitter-logo" href="#"></a>
								<a class="icon icon-instagram-logo" href="#"></a>
							</div>
						</div>
					</div><div class="col-sm-6 col-md-3 slick-slide slick-active" data-slick-index="1" aria-hidden="false" style="width: 300px;">
						<div class="person">
							<div class="image image-scale-color"><img src="http://serrallerialeixample.es/new/theme/theme/images/person-01.jpg" alt="" class="img-responsive" style="">
								<div class="hover"></div>
							</div>
							<h5 class="name" style="">Carles Dominguez</h5>
							<h6 class="position" style="">Tècnic</h6>
							<div class="text">Estic especialitzat en automatismes d'alta seguretat. Aconseguir una llar segura és l'objectiu de la nostra empresa.
							</div>
							<div class="link">
								<a class="icon icon-facebook-logo" href="#"></a>
								<a class="icon icon-twitter-logo" href="#"></a>
								<a class="icon icon-instagram-logo" href="#"></a>
							</div>
						</div>
					</div><div class="col-sm-6 col-md-3 slick-slide slick-active" data-slick-index="2" aria-hidden="false" style="width: 300px;">
						<div class="person">
							<div class="image image-scale-color"><img src="http://serrallerialeixample.es/new/theme/theme/images/person-03.jpg" alt="" class="img-responsive" style="">
								<div class="hover"></div>
							</div>
							<h5 class="name" style="">Robert T. Kennelly</h5>
							<h6 class="position" style="">Certified Technician</h6>
							<div class="text">We use only quality parts because we never compromise on the quality of our service.</div>
							<div class="link">
								<a class="icon icon-facebook-logo" href="#"></a>
								<a class="icon icon-twitter-logo" href="#"></a>
								<a class="icon icon-instagram-logo" href="#"></a>
							</div>
						</div>
					</div><div class="col-sm-6 col-md-3 slick-slide slick-active" data-slick-index="3" aria-hidden="false" style="width: 300px;">
						<div class="person">
							<div class="image image-scale-color"><img src="http://serrallerialeixample.es/new/theme/theme/images/person-04.jpg" alt="" class="img-responsive" style="">
								<div class="hover"></div>
							</div>
							<h5 class="name" style="">Kevin C. Goodwin</h5>
							<h6 class="position" style="">General Manager</h6>
							<div class="text">We use only quality parts because we never compromise on the quality of our service.</div>
							<div class="link">
								<a class="icon icon-facebook-logo" href="#"></a>
								<a class="icon icon-twitter-logo" href="#"></a>
								<a class="icon icon-instagram-logo" href="#"></a>
							</div>
						</div>
					</div></div></div>
					
					
					
				</div>
			</div>
		</div>
		<!-- //Team Block -->
	</div>
	<div>[footer]</div>