

    
<!-- Content  -->
	

	<!-- Content  -->
	

	
<!-- Slider -->
		
		<!-- Slider -->
		<!-- Content  -->
		
		<!-- // Content  -->
		
			<!-- //Block -->
			<!-- How It Works -->
			
			<!-- //How It Works -->
			<!-- Services Block -->
			
			<!-- //Services Block -->
			<!-- Testimonials Block -->
			
			<!-- //Testimonials Block -->
			<!-- Statistics Block -->
			
			<!-- //Statistics Block -->
			<!-- Recalls Block -->
			
			<!-- //Recalls Block -->
			<!-- Appointment Block -->
			
			<!-- //Appointment Block --><!-- Content  -->
	

	
<!-- Slider -->
		
		<!-- Slider -->
		<!-- Content  -->
		
		<!-- // Content  -->
		
			<!-- //Block -->
			<!-- How It Works -->
			
			<!-- //How It Works -->
			<!-- Services Block -->
			
			<!-- //Services Block -->
			<!-- Testimonials Block -->
			
			<!-- //Testimonials Block -->
			<!-- Statistics Block -->
			
			<!-- //Statistics Block -->
			<!-- Recalls Block -->
			
			<!-- //Recalls Block -->
			<!-- Appointment Block -->
			
			<!-- //Appointment Block -->
<!-- Content  -->
		
		
		<!-- // Content  -->
<!-- Content  -->
	

	
<!-- Slider -->
		
		<!-- Slider -->
		<!-- Content  -->
		
		<!-- // Content  -->
		
			<!-- //Block -->
			<!-- How It Works -->
			
			<!-- //How It Works -->
			<!-- Services Block -->
			
			<!-- //Services Block -->
			<!-- Testimonials Block -->
			
			<!-- //Testimonials Block -->
			<!-- Statistics Block -->
			
			<!-- //Statistics Block -->
			<!-- Recalls Block -->
			
			<!-- //Recalls Block -->
			<!-- Appointment Block -->
			
			<!-- //Appointment Block -->
<!-- Content  -->
		
		
		<!-- // Content  -->
<!-- Footer -->
		<div class="page-footer" id="contacte">
			<div class="footer-content">
				<div class="footer-col-left">
					<div class="inside">
						<div class="footer-phone">
							<h2 class="h-phone" style="">
								Telèfons de contacte: 
								<br>
								<span class="number">
									<br><i class="fa fa-mobile" style="color:white"></i> <a href="tel:+34691802758" style="text-decoration: none" class="color"> &nbsp; 691 802 758 </a>
									<br><i class="fa fa-phone" style="color:white"></i> <a href="tel:+34691802758" style="text-decoration: none" class="color">  936 670 694</a>
								</span>
							</h2>
						</div>
						<div class="contact-info  editContent" style="">
							<i class="icon icon-locate"></i>C/ Girona, 134 Barcelona
							<br> CP 08037
						</div>
						<div class="contact-info  editContent" style="">
							<i class="icon icon-clock"></i>
							Dill-Div <span class="color">9:30h - 13:30h / 16:30h - 20:00h</span>
							
						</div>
						<div class="contact-info">
							<i class="icon fa fa-envelope"></i>
							<a href="mailto:serralleria@serrallerialeixample.es" style="color:white">
								serralleria@serrallerialeixample.es
							</a>
						</div>
						<div class="social-links">
							<ul>
								<li>
									<a class="icon icon-facebook-logo" href="https://www.facebook.com/serrallerialeixample.es/"></a>
								</li>
																<li>
									<a class="icon icon-instagram-logo" href="https://www.instagram.com/serralleriaeixample/"></a>
								</li>
								
							</ul>
						</div>
					</div>
				</div>
				<div class="footer-col-right">
					<div id="footer-map" class="footer-map" style="position: relative; overflow: hidden;"><div style="height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; background-color: rgb(229, 227, 223);"><div style="overflow: hidden;"></div><div style="position: absolute; z-index: 0; left: 0px; top: 0px; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px;" class="gm-style"><div style="position: absolute; z-index: 0; left: 0px; top: 0px; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; cursor: url(&quot;https://maps.gstatic.com/mapfiles/openhand_8_8.cur&quot;), default; touch-action: pan-x pan-y;" tabindex="0"><div style="z-index: 1; position: absolute; left: 50%; top: 50%;"><div style="position: absolute; left: 0px; top: 0px; z-index: 100; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 101; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 102; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 103; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: -1;"></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"></div></div><div style="z-index: 2; position: absolute; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; left: 0px; top: 0px; opacity: 0;" class="gm-style-pbc"><p class="gm-style-pbt"></p></div><div style="z-index: 3; position: absolute; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; left: 0px; top: 0px; touch-action: pan-x pan-y;"><div style="z-index: 4; position: absolute; left: 50%; top: 50%;"><div style="position: absolute; left: 0px; top: 0px; z-index: 104; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 105; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 106; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 107; width: 100%;"></div></div></div></div><iframe style="z-index: -1; position: absolute; width: 100%; height: 100%; top: 0px; left: 0px; border: medium none;" src="about:blank" frameborder="0"></iframe></div></div></div>
				</div>
			</div>
			<div class="footer-bottom">
				<div class="container">
					<div class="copyright editContent" style="">© 2018 Serralleria L'Eixample, Tots els drets reservats</div>
				</div>
			</div>
		</div>
		<!-- //Footer -->