<div>[header]</div>
<!-- Slider -->
		<div id="mainSliderWrapper">
			<div id="mainSlider">
				<div class="slide">
					<div class="img--holder" style="background-image: url([base_url]theme/theme/images/slider/slide1.jpg);"></div>
					<div class="slide-content center">
						<div class="vert-wrap container">
							<div class="vert">
								<div class="container">
									<h4 data-animation="zoomIn" data-animation-delay="0.5s">La porta de casa teva no s'obre?</h4>
									<h3 data-animation="scaleOut" data-animation-delay="0.2s">Accedim on necessitis</h3>
									<p data-animation="fadeIn" data-animation-delay="0.9s">Reparem i canviem els panys bloquejats</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="slide">
					<div class="img--holder" style="background-image: url([base_url]theme/theme/images/slider/slide2.jpg);"></div>
					<div class="slide-content container left">
						<div class="vert-wrap">
							<div class="vert">
								<div class="container">
									<h4 data-animation="fadeInLeft" data-animation-delay="0.2s">Servei 24 hores</h4>
									<h3 data-animation="flipInX" data-animation-delay="0.8s">Manteniment, reparació..</h3>
									<h3 data-animation="flipInX" data-animation-delay="1.2s"></h3>
									<p data-animation="fadeIn" data-animation-delay="1.5s">Ens trobaràs sempre que ens necessitis</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="slide">
					<div class="img--holder" style="background-image: url([base_url]theme/theme/images/slider/slide3.jpg);"></div>
					<div class="slide-content container center">
						<div class="vert-wrap">
							<div class="vert">
								<div class="container">
									<h4 data-animation="zoomIn" data-animation-delay="0.8s">Serrallers amb molta experiència</h4>
									<h3 data-animation="fadeInUp" data-animation-delay="0.2s">Panys, comunitats, persianes, claus...</h3>
									<h3 data-animation="fadeInUp" data-animation-delay="0.5s"></h3>
									<p data-animation="fadeIn" data-animation-delay="1.2s">Més de 15 anys en serralleria</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Slider -->
		<!-- Content  -->
		<div id="pageContent">
			<!-- Under Slider Banner -->
			<div class="block banner-under-slider">
				<div class="container">
					<div class="row">
						<div class="col-sm-8 col-md-6">
							<div class="row">
								<div class="col-md-6">
									<h2>Servei Serralleria</h2>
									<h2 class="h-lg text-right"><span class="color">24 hores</span></h2>
								</div>
								<div class="col-xs-9 col-md-6">
									<p>Contacta amb nosaltres quan necessitis, tenim un servei d'emergències a la teva disposició</p>
								</div>
							</div>
						</div>
						<div class="col-sm-4 col-md-6">
							<div class="row">
								<div class="col-md-6 col-lg-8">
									<div class="negative-margin"><img src="[base_url]theme/theme/images/banner-key.png" class="img-responsive" alt=""></div>
								</div>
								<div class="col-md-6 col-lg-4 action hidden-xs">
									<a href="tel:+34691802758" class="btn btn-full btn-border"><span>TRUCA'NS ARA</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- // Under Slider Banner -->
			<!-- Services Banner Block -->
			<div class="block">
				<div class="container">
					<div class="text-center">
						<h2 class="h-lg">Què fem</h2>
						<p class="info">Oferim servei complet de reparació i manteniment de serralleria</p>
					</div>
					<div class="services-block services-carousel">
						<div class="service">
							<div class="image"><img src="[base_url]theme/theme/images/service-1-bg.png" alt="#"></div>
							<div class="caption">
								<div class="vert-wrap">
									<div class="vert">
										<h3>Panys i bombins </h3>
										<h2>de Seguretat</h2>
										<div class="text">Instal·lem, reparem, obrim i canviem panys d'alta seguretat
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="service">
							<div class="image image-scale"><img src="[base_url]theme/theme/images/service-2-bg.jpg" alt="#"></div>
						</div>
						<div class="service">
							<div class="image"><img src="[base_url]theme/theme/images/service-3-bg.jpg" alt="#"></div>
							<div class="caption">
								<div class="vert-wrap">
									<div class="vert">
										<h3>Duplicat de</h3>
										<h2>Claus i comandaments</h2>
										<div class="text">Claus d'alta seguretat
											<br>i comandaments de cotxe
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="service">
							<div class="image image-scale"><img src="[base_url]theme/theme/images/service-4-bg.jpg" alt="#"></div>
						</div>
						<div class="service dark">
							<div class="image"><img src="[base_url]theme/theme/images/service-5-bg.jpg" alt="#"></div>
							<div class="caption">
								<div class="vert-wrap">
									<div class="vert">
										<h3>Portes, Reixes i</h3>
										<h2>Persianes</h2>
										<div class="text">Instal·lem i reparem
											<br> tot tipus de tancaments
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="service">
							<div class="image image-scale"><img src="[base_url]theme/theme/images/service-6-bg.jpg" alt="#"></div>
						</div>
					</div>
				</div>
			</div>
			<!-- //Services Banner Block -->
			<!-- Services List Block -->
			<div class="block bg-1" id="serveis">
				<div class="container">
					<h2 class="h-lg text-center">Els nostres <span class="color">Serveis</span></h2>
					<p class="info text-center">A continuació es detallen alguns dels molts serveis que oferim:</p>
					<div class="row" id="slideMobile">
						<div class="col-sm-4 col-md-4">
							<ul class="marker-list">
								<li>Servei d'obertura de portes</li>
								<li>Servei d'obertura de vehícles</li>
								<li>Servei d'obertura de caixes fortes</li>
								<li>Instal·lació de tot tipus de panys</li>
								
							</ul>
						</div>
						<div class="col-sm-4 col-md-4 view-more-mobile">
							<ul class="marker-list">
								<li>Substitució de tot tipus de panys</li>
								<li>Reparació de tot tipus de panys</li>
								<li>Manteniment de comunitats</li>
								<li>Instal·lacions de persianes</li>
								
								</li>
							</ul>
						</div>
						<div class="col-sm-4 col-md-4 view-more-mobile">
							<ul class="marker-list">
								<li>Reparació de persianes</li>
								<li>Duplicat de claus</li>
								<li>Clonament de comandaments</li>
								<li>Servei urgent 24 hores</li>
								
							</ul>
						</div>
					</div>
					<div class="text-center"><a href="#slideMobile" class="view-more-link color"><span class="more">All Services</span><span class="less">Hide All Services</span></a>
					</div>
				</div>
			</div>
			<!-- //Services List Block -->
			
		</div>
		<!-- // Content  -->
		<div class="block">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-lg-5">
							<div class="vertical-tab-container" id="tabForm">
								<div class="vertical-tab-menu">
									<div class="list-group">
										<a href="#" class="list-group-item active text-center">
										<span>Portes</span>
										<img src="[base_url]theme/theme/images/rueda.png" alt="" style="max-width:50px">
										</a>
										<a href="#" class="list-group-item text-center">
										<span>Persianes</span>
										<img src="[base_url]theme/theme/images/persianes.png" alt="" style="max-width:50px">
										</a>
										<a href="#" class="list-group-item text-center">
										<span>Reixes</span>
										<img src="[base_url]theme/theme/images/reixes.png" alt="" style="max-width:50px">
										</a>
										<a href="#" class="list-group-item text-center">
										<span>Panys</span>
										<img src="[base_url]theme/theme/images/panys.png" alt="" style="max-width:50px">
										</a>
										<a href="#" class="list-group-item text-center">
										<span>Comunitats</span>
										<img src="[base_url]theme/theme/images/comunitats.png" alt="" style="max-width:50px">
										</a>
									</div>
								</div>
								<div class="vertical-tab">
									<!-- Tires section -->
									<div class="vertical-tab-content active">
										<h3>Demana el teu pressupost</h3>
										<div class="clearfix">
											<a href="#tabForm" class="toggle-btn active">Persianes</a><a href="#tabForm" class="toggle-btn">Portes</a>
										</div>
										<p class="comment">Tots els camps són obligatoris</p>
										<form id="tab1-form">
											<div class="form-group">
												<div class="select-wrapper">
													<select name="select1" class="input-custom">
														<option value="">Tipus</option>
														<option value="">Entrada</option>
														<option value="">Garaig</option>
														<option value="">Bàsica</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<div class="select-wrapper">
													<select name="select2" class="input-custom">
														<option value="">Mides</option>
														<option value="">2x3</option>
														<option value="">2x5</option>
														<option value="">2x8</option>
														<option value="">2x9</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<div class="select-wrapper">
													<select name="select3" class="input-custom">
														<option value="">Material</option>
														<option value="">Alumini</option>
														<option value="">Fusta</option>
														<option value="">xx</option>
														<option value="">xx</option>
														<option value="">xxL</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<div class="select-wrapper">
													<select name="select4" class="input-custom">
														<option value="">Tancament</option>
														<option value="">Bàsic</option>
														<option value="">Doble tanca</option>
														<option value="">Codi</option>
														<option value="">Sensor</option>
													</select>
												</div>
											</div>
											<div class="row">
												<div class="col-xs-7">
													<div class="form-group">
														<div class="select-wrapper">
															<select name="select5" class="input-custom">
																<option value="">TPMS</option>
																<option value="">RDKS</option>
															</select>
														</div>
													</div>
												</div>
												<div class="col-xs-5"><a href="#" class="comment">Què és?</a></div>
											</div>
											<div class="form-group">
												<div class="row">
													<div class="col-xs-7">
														<div class="form-group">
															<div class="select-wrapper">
																<select name="select6" class="input-custom">
																	<option value="">60605</option>
																	<option value="">20105</option>
																</select>
															</div>
														</div>
													</div>
													<div class="col-xs-5"><a href="#" class="comment">Per què?</a></div>
												</div>
											</div>
											<a class="btn btn-lg btn-full" data-toggle="modal" data-target="#tab1Modal"><span>Demanar preu</span></a>
										</form>
									</div>
									<!-- Oil section -->
									<div class="vertical-tab-content">
										<h3>Get the Right Oil</h3>
										<div class="clearfix">
											<a href="#tabForm" class="toggle-btn active">By Vehicle</a><a href="#tabForm" class="toggle-btn">By Tire</a>
										</div>
										<p class="comment">All fields are required</p>
										<form id="tab2-form">
											<div class="form-group">
												<div class="select-wrapper">
													<select name="select1" class="input-custom">
														<option value="">Year</option>
														<option value="">2010</option>
														<option value="">2015</option>
														<option value="">2016</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<div class="select-wrapper">
													<select name="select2" class="input-custom">
														<option value="">Make</option>
														<option value="">Ford</option>
														<option value="">Audi</option>
														<option value="">Honda</option>
														<option value="">Toyota</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<div class="select-wrapper">
													<select name="select3" class="input-custom">
														<option value="">Model</option>
														<option value="">Escort</option>
														<option value="">Prius</option>
														<option value="">Camaro</option>
														<option value="">Civic</option>
														<option value="">240DL</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<div class="select-wrapper">
													<select name="select4" class="input-custom">
														<option value="">Submodel</option>
														<option value="">Sedan</option>
														<option value="">Hatchback</option>
														<option value="">Regular Cab</option>
														<option value="">Crew Cab</option>
													</select>
												</div>
											</div>
											<div class="row">
												<div class="col-xs-7">
													<div class="form-group">
														<div class="select-wrapper">
															<select name="select5" class="input-custom">
																<option value="">TPMS</option>
																<option value="">RDKS</option>
															</select>
														</div>
													</div>
												</div>
												<div class="col-xs-5"><a href="#" class="comment">What’s this?</a></div>
											</div>
											<div class="form-group">
												<div class="row">
													<div class="col-xs-7">
														<div class="form-group">
															<div class="select-wrapper">
																<select name="select6" class="input-custom">
																	<option value="">60605</option>
																	<option value="">20105</option>
																</select>
															</div>
														</div>
													</div>
													<div class="col-xs-5"><a href="#" class="comment">Why?</a></div>
												</div>
											</div>
											<a class="btn btn-lg btn-full" data-toggle="modal" data-target="#tab2Modal"><span>Get Oil Pricing</span></a>
										</form>
									</div>
									<!-- Batteries section -->
									<div class="vertical-tab-content">
										<h3>Select the Model</h3>
										<div class="clearfix">
											<a href="#tabForm" class="toggle-btn active">By Vehicle</a><a href="#tabForm" class="toggle-btn">By Tire</a>
										</div>
										<p class="comment">All fields are required</p>
										<form id="tab3-form">
											<div class="form-group">
												<div class="select-wrapper">
													<select name="select1" class="input-custom">
														<option value="">Year</option>
														<option value="">2010</option>
														<option value="">2015</option>
														<option value="">2016</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<div class="select-wrapper">
													<select name="select2" class="input-custom">
														<option value="">Make</option>
														<option value="">Ford</option>
														<option value="">Audi</option>
														<option value="">Honda</option>
														<option value="">Toyota</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<div class="select-wrapper">
													<select name="select3" class="input-custom">
														<option value="">Model</option>
														<option value="">Escort</option>
														<option value="">Prius</option>
														<option value="">Camaro</option>
														<option value="">Civic</option>
														<option value="">240DL</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<div class="select-wrapper">
													<select name="select4" class="input-custom">
														<option value="">Submodel</option>
														<option value="">Sedan</option>
														<option value="">Hatchback</option>
														<option value="">Regular Cab</option>
														<option value="">Crew Cab</option>
													</select>
												</div>
											</div>
											<div class="row">
												<div class="col-xs-7">
													<div class="form-group">
														<div class="select-wrapper">
															<select name="select5" class="input-custom">
																<option value="">TPMS</option>
																<option value="">RDKS</option>
															</select>
														</div>
													</div>
												</div>
												<div class="col-xs-5"><a href="#" class="comment">What’s this?</a></div>
											</div>
											<div class="form-group">
												<div class="row">
													<div class="col-xs-7">
														<div class="form-group">
															<div class="select-wrapper">
																<select name="select6" class="input-custom">
																	<option value="">60605</option>
																	<option value="">20105</option>
																</select>
															</div>
														</div>
													</div>
													<div class="col-xs-5"><a href="#" class="comment">Why?</a></div>
												</div>
											</div>
											<a class="btn btn-lg btn-full" data-toggle="modal" data-target="#tab3Modal"><span>Get Batteries Pricing</span></a>
										</form>
									</div>
									<!-- Brakes section -->
									<div class="vertical-tab-content">
										<h3>Select the Model</h3>
										<div class="clearfix">
											<a href="#tabForm" class="toggle-btn active">By Vehicle</a><a href="#tabForm" class="toggle-btn">By Tire</a>
										</div>
										<p class="comment">All fields are required</p>
										<form id="tab4-form">
											<div class="form-group">
												<div class="select-wrapper">
													<select name="select1" class="input-custom">
														<option value="">Year</option>
														<option value="">2010</option>
														<option value="">2015</option>
														<option value="">2016</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<div class="select-wrapper">
													<select name="select2" class="input-custom">
														<option value="">Make</option>
														<option value="">Ford</option>
														<option value="">Audi</option>
														<option value="">Honda</option>
														<option value="">Toyota</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<div class="select-wrapper">
													<select name="select3" class="input-custom">
														<option value="">Model</option>
														<option value="">Escort</option>
														<option value="">Prius</option>
														<option value="">Camaro</option>
														<option value="">Civic</option>
														<option value="">240DL</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<div class="select-wrapper">
													<select name="select4" class="input-custom">
														<option value="">Submodel</option>
														<option value="">Sedan</option>
														<option value="">Hatchback</option>
														<option value="">Regular Cab</option>
														<option value="">Crew Cab</option>
													</select>
												</div>
											</div>
											<div class="row">
												<div class="col-xs-7">
													<div class="form-group">
														<div class="select-wrapper">
															<select name="select5" class="input-custom">
																<option value="">TPMS</option>
																<option value="">RDKS</option>
															</select>
														</div>
													</div>
												</div>
												<div class="col-xs-5"><a href="#" class="comment">What’s this?</a></div>
											</div>
											<div class="form-group">
												<div class="row">
													<div class="col-xs-7">
														<div class="form-group">
															<div class="select-wrapper">
																<select name="select6" class="input-custom">
																	<option value="">60605</option>
																	<option value="">20105</option>
																</select>
															</div>
														</div>
													</div>
													<div class="col-xs-5"><a href="#" class="comment">Why?</a></div>
												</div>
											</div>
											<a class="btn btn-lg btn-full" data-toggle="modal" data-target="#tab4Modal"><span>Get Brakes Pricing</span></a>
										</form>
									</div>
									<!-- Alignment section -->
									<div class="vertical-tab-content">
										<h3>Select the Model</h3>
										<div class="clearfix">
											<a href="#tabForm" class="toggle-btn active">By Vehicle</a><a href="#tabForm" class="toggle-btn">By Tire</a>
										</div>
										<p class="comment">All fields are required</p>
										<form id="tab5-form">
											<div class="form-group">
												<div class="select-wrapper">
													<select name="select1" class="input-custom">
														<option value="">Year</option>
														<option value="">2010</option>
														<option value="">2015</option>
														<option value="">2016</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<div class="select-wrapper">
													<select name="select2" class="input-custom">
														<option value="">Make</option>
														<option value="">Ford</option>
														<option value="">Audi</option>
														<option value="">Honda</option>
														<option value="">Toyota</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<div class="select-wrapper">
													<select name="select3" class="input-custom">
														<option value="">Model</option>
														<option value="">Escort</option>
														<option value="">Prius</option>
														<option value="">Camaro</option>
														<option value="">Civic</option>
														<option value="">240DL</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<div class="select-wrapper">
													<select name="select4" class="input-custom">
														<option value="">Submodel</option>
														<option value="">Sedan</option>
														<option value="">Hatchback</option>
														<option value="">Regular Cab</option>
														<option value="">Crew Cab</option>
													</select>
												</div>
											</div>
											<div class="row">
												<div class="col-xs-7">
													<div class="form-group">
														<div class="select-wrapper">
															<select name="select5" class="input-custom">
																<option value="">TPMS</option>
																<option value="">RDKS</option>
															</select>
														</div>
													</div>
												</div>
												<div class="col-xs-5"><a href="#" class="comment">What’s this?</a></div>
											</div>
											<div class="form-group">
												<div class="row">
													<div class="col-xs-7">
														<div class="form-group">
															<div class="select-wrapper">
																<select name="select6" class="input-custom">
																	<option value="">60605</option>
																	<option value="">20105</option>
																</select>
															</div>
														</div>
													</div>
													<div class="col-xs-5"><a href="#" class="comment">Why?</a></div>
												</div>
											</div>
											<a class="btn btn-lg btn-full" data-toggle="modal" data-target="#tab5Modal"><span>Get Pricing</span></a>
										</form>
									</div>
								</div>
							</div>
						</div>
						<div class="divider-lg visible-sm visible-xs"></div>
						<div class="col-md-6 col-lg-7">
							<h2>Tots el serveis a la teva disposició</h2>
							<p>Si vols garantir la seguretat de la teva llar nosaltres et podem fe run pressupost a mida. Ens encarreguem d'instal·lar-te tots els tancaments i assessorar-te perquè estiguis tranquil.</p>
							<ul class="marker-list-sm">
								<li>Panys codificats</li>
								<li>Claus d'alta seguretat</li>
								<li>Tancaments automàtics</li>
								<li>Controls a distància</li>
								<li>Reixes</li>
								<li>Sensors</li>
							</ul>
							<div class="promo-banner">
								<div class="row">
									<div class="col-lg-8">
										<div class="text">
											<div class="title"><span>25%</span> de Descompte!</div>
											<p>Aconsegueix un 25% de descompte demanant aquí el teu pressupost. </p>
										</div>
									</div>
									<div class="divider hidden-lg"></div>
									<div class="col-lg-4">
										<div class="action"> <i class="icon icon-lg icon-arrows-2"></i>
											<a href="#" class="btn btn-lg"><span>Demanar!</span></a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- //Block -->
			<!-- How It Works -->
			<div class="block bg-2">
				<div class="container">
					<h2 class="h-lg text-center">Els nostres <span class="color">punts forts</span></h2>
					<div class="row how-works-row shifted">
						<div class="col-sm-6 col-md-3">
							<div class="how-works-block">
								<div class="image">
									<div class="image-scale"><img src="http://serrallerialeixample.es/new/theme/theme/images/how-works-img-1.jpg" alt=""></div>
								</div>
								<div class="caption">
									<div class="inside">
										<div class="number"><span>1</span></div>
										<div class="text">
											<h5>Màxima rapidesa</h5>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-md-3">
							<div class="how-works-block">
								<div class="image">
									<div class="image-scale"><img src="http://serrallerialeixample.es/new/theme/theme/images/how-works-img-2.jpg" alt=""></div>
								</div>
								<div class="caption">
									<div class="inside">
										<div class="number"><span>2</span></div>
										<div class="text">
											<h5>Màxima qualitat</h5>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-md-3">
							<div class="how-works-block">
								<div class="image">
									<div class="image-scale"><img src="http://serrallerialeixample.es/new/theme/theme/images/how-works-img-3.jpg" alt=""></div>
								</div>
								<div class="caption">
									<div class="inside">
										<div class="number"><span>3</span></div>
										<div class="text">
											<h5>Totes les solucions</h5>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-md-3">
							<div class="how-works-block">
								<div class="image">
									<div class="image-scale"><img src="http://serrallerialeixample.es/new/theme/theme/images/how-works-img-4.jpg" alt=""></div>
								</div>
								<div class="caption">
									<div class="inside">
										<div class="number"><span>4</span></div>
										<div class="text">
											<h5>Clients satisfets</h5>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- //How It Works -->
			<!-- Services Block -->
			<div class="block">
				<div class="container">
					<h2 class="h-lg text-center">Els nostres certificats oficials</h2>
					<p class="info text-center">Som un equips de tècnics i manyàs especialitzats en tancaments de seguretat</p>
					<div class="row text-icon-carousel">
						<div class="col-sm-4 col-md-4">
							<div class="text-icon">
								<div class="icon-wrapper"><span><i class="icon icon-technology"></i><span class="icon-hover"></span></span>
								</div>
								<h3 class="title">Codificacions</h3>
								<p class="text">Disposem d'una tecnologia específica per al xifrat i codificació dels últims panys electrònics del mercat</p>
							</div>
						</div>
						<div class="col-sm-4 col-md-4">
							<div class="text-icon">
								<div class="icon-wrapper"><span><i class="icon icon-tool"></i><span class="icon-hover"></span></span>
								</div>
								<h3 class="title">DUPLICATS</h3>
								<p class="text">Realitzem els duplicats de les teves claus i comandaments sense errors. Treballem amb codis CCR per evitar imperfeccions.
								</p>
							</div>
						</div>
						<div class="col-sm-4 col-md-4">
							<div class="text-icon">
								<div class="icon-wrapper"><span><i class="icon icon-diploma"></i><span class="icon-hover"></span></span>
								</div>
								<h3 class="title">Certificats</h3>
								<p class="text">Tots els nostres tècnics han pasat un exàmen tècnic.
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- //Services Block -->
			<!-- Testimonials Block -->
			<div class="block bg-3">
				<div class="container-fluid">
					<h2 class="h-lg text-center">Què <span class="color">opinen</span> els nostres clients</h2>
					<div class="testimonials">
						<div class="testimonials-item">
							<div class="inside">
								<div class="meta"><span class="rating"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i></span><span class="username">Josep Lane</span></div>
								<div class="text">Van entrar a robar a casa i en menys de 30 minuts tenia el tènic allà reparant-se la destrossa del pany. A més em va assessorar per augmentar la seguretat de la meva casa. Un servei excel·lent!</div>
							</div>
							<div class="bg-image" style="background-image: url(images/testimonial-bg-1.jpg);"></div>
						</div>
						<div class="testimonials-item">
							<div class="inside">
								<div class="meta"><span class="rating"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i></span><span class="username">Carles Alonso</span></div>
								<div class="text">Vivim en una comunitat de veïns que és un caos, tenim molts accessos exteriors i Serralleria l'Eixample ens ha instal·lat un sistema de seguretat que ens fa viure més tranquils. A més cada mes ve el tècnic a fer el manteniment de tots els tancaments. 100% recomanable!.</div>
							</div>
							<div class="bg-image" style="background-image: url(images/testimonial-bg-2.jpg);"></div>
						</div>
					</div>
				</div>
			</div>
			<!-- //Testimonials Block -->
			<!-- Statistics Block -->
			<div class="block">
				<div class="container">
					<h2 class="h-lg text-center">Algunes xifres <span class="color">sobre nosaltres</span></h2>
					<p class="info text-center">Procurem sempre aconseguir els millors resultats i satisfer als nostres clients</p>
					<div class="row" id="counterBlock">
						<div class="col-sm-6 col-md-3">
							<div class="stat-box">
								<div><span class="number"><span class="count" data-to="10" data-speed="1000">10</span></span><span class="icon"><i class="icon-rocket"></i></span></div>
								<div class="text">
									<h5>Anys d'experiència</h5>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-md-3">
							<div class="stat-box">
								<div><span class="number"><span class="count" data-to="32" data-speed="1000">32</span></span><span class="icon"><i class="icon-people-1"></i></span></div>
								<div class="text">
									<h5>professionals en serralleria</h5>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-md-3">
							<div class="stat-box">
								<div><span class="number"><span class="count" data-to="2500" data-speed="1000">2500</span></span><span class="icon"><i class="icon-people"></i></span></div>
								<div class="text">
									<h5>clients satisfets</h5>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-md-3">
							<div class="stat-box">
								<div><span class="number"><span class="count" data-to="1900" data-speed="1000">1900</span></span><span class="icon"><i class="icon-transport"></i></span></div>
								<div class="text">
									<h5>panys reparats</h5>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- //Statistics Block -->
			<!-- Recalls Block -->
			<div class="block bg-dark full-block pad-sm">
				<div class="container">
					<div class="text-center">
						<h2 class="h-lg">Demana <span class="color"></span> pressupost</h2>
						<p class="info">Envia'ns el que necessites i contactarem amb tu lo abans possible
						</p>
					</div>
					<form action="#" class="form-table">
						<div class="form-group form-group-cell">
							<div class="select-wrapper">
								<select  class="input-custom">
									<option value="">Tancaments</option>
									<option value="">Reparació</option>
								</select>
							</div>
						</div>
						<div class="form-group form-group-cell">
							<div class="select-wrapper">
								<select  class="input-custom">
									<option value="">Porta</option>
									<option value="">Persiana</option>
								</select>
							</div>
						</div>
						<div class="form-group form-group-cell sm">
							<div class="select-wrapper">
								<select  class="input-custom">
									<option value="">Material</option>
									<option value="">2015</option>
								</select>
							</div>
						</div>
						<div class="form-group form-group-cell sm">
							<input type="text" class="form-control input-custom" placeholder="ZIP">
						</div>
						<div class="form-group form-group-cell action">
							<a class="btn btn-full btn-invert" data-toggle="modal" data-target="#getRecallsModal"><span>Enviar</span></a>
						</div>
					</form>
				</div>
			</div>
			<!-- //Recalls Block -->
			<!-- Appointment Block -->
			<div class="block">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<div class="text-appointment">
								<h2 class="h-lg">Urgències <span class="color">24h</span></h2>
								<p class="info">Truca'ns i solucionarem el teu problema!</p>
								<h2 class="h-phone">691 802 758</h2>
								<div class="btn-inline"><a class="btn btn-invert" href="#"><span>Necessites ajuda?</span></a><a class="btn" href="contact.html"><span>Demanar pressupost</span></a></div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="img-move animation" data-animation="fadeInRight" data-animation-delay="0s">
								<img src="http://serrallerialeixample.es/new/theme/theme/images/img-car-move.png" alt="">
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- //Appointment Block -->
		</div>
		<div>[footer]</div>