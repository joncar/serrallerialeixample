<div>[header]</div>
		<div id="pageTitle">
			<div class="container">
				<!-- Breadcrumbs Block -->
				<div class="breadcrumbs">
					<ul class="breadcrumb">
						<li><a href="index.html">Inici</a></li>
						<li class="active">Preguntes freqüents</li>
					</ul>
				</div>
				<!-- //Breadcrumbs Block -->
				<h1>Preguntes <span class="color">Freqüents</span></h1>
			</div>
		</div>
		<div id="pageContent">
			<!-- Panel Block -->
			<div class="block offset-sm">
				<div class="container">
					<p>En aquest llistat intentem resoldre els teus dubtes. En cas de no aconseguir-ho ens pots trucar i trobarem una solució.</p>
					<div class="panel-group">
						<div class="faq-item">
							<div class="panel">
								<div class="panel-heading">
									<h4 class="panel-title"><a role="button" data-toggle="collapse" href="#faq1">És cert que existeixen claus mestres que obren tot tipus de panys?<span class="caret-toggle closed">–</span><span class="caret-toggle opened">+</span></a></h4>
								</div>
								<div id="faq1" class="panel-collapse collapse in">
									<div class="panel-body">
										<p>No. No és cert. Però si que és cert que els lladres usen una tècnica per obrir panys coneguda com bumping. La tècnica consisteix a inserir una clau realitzada amb la posició més baixa a la qual arriben els pistons i copejar-la amb un objecte de forma repetida , separant així els pistons dels contrapistones, alliberant així el gir de la clau. Avui dia ja existeix barrets forts d'alta seguretat que eviten aquesta tècnica d'obertura.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="faq-item">
							<div class="panel">
								<div class="panel-heading">
									<h4 class="panel-title"><a role="button" data-toggle="collapse" href="#faq2">Quins panys són els més segurs per evitar robatoris?<span class="caret-toggle closed">–</span><span class="caret-toggle opened">+</span></a></h4>
								</div>
								<div id="faq2" class="panel-collapse collapse in">
									<div class="panel-body">
										<p>Existeixen moltes marques de panys que s'esforcen cada dia a oferir la màxima seguretat possible per evitar robatoris. Nosaltres fem estudis personalitzats gratuïts en la seva porta per oferir-li la millor opció de seguretat en funció del que ja disposa. Nosaltres treballem amb les millors marques en seguretat i li instal·lem el pany d'alta seguretat que adapti al perfil de la seva porta.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="faq-item">
							<div class="panel">
								<div class="panel-heading">
									<h4 class="panel-title"><a role="button" data-toggle="collapse" href="#faq3">Vull més seguretat a la meva porta. Què puc fer?<span class="caret-toggle closed">–</span><span class="caret-toggle opened">+</span></a></h4>
								</div>
								<div id="faq3" class="panel-collapse collapse in">
									<div class="panel-body">
										<p>El millor que pot fer és trucar-nos i sol·licitar un estudi personalitzat per augmentar la seguretat sense necessitat de canviar la porta.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="faq-item">
							<div class="panel">
								<div class="panel-heading">
									<h4 class="panel-title"><a role="button" data-toggle="collapse" href="#faq4">Puc instal·lar jo un pany?<span class="caret-toggle closed">–</span><span class="caret-toggle opened">+</span></a></h4>
								</div>
								<div id="faq4" class="panel-collapse collapse in">
									<div class="panel-body">
										<p>Sens dubte és millor comptar amb un manyà. Els nostres manyans disposen de les eines adequades per a cada marca de panys i és recomanable tenir el coneixement adequat per canviar un pany ja que cometre un error podria encarir el servei per haver d'emprar-se mes temps per a la reparació en si.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="faq-item">
							<div class="panel">
								<div class="panel-heading">
									<h4 class="panel-title"><a role="button" data-toggle="collapse" href="#faq5">Tinc una porta blindada. És segura?<span class="caret-toggle closed">–</span><span class="caret-toggle opened">+</span></a></h4>
								</div>
								<div id="faq5" class="panel-collapse collapse in">
									<div class="panel-body">
										<p>El que garanteix la seguretat d'una porta blindada independentment de la porta en si és el barret fort del pany. Vostè ha d'assegurar-se que disposa d'un barret fort de seguretat anti robatori. El tenir un pany blindat no significa que disposi d'un bombillo de seguretat. Les portes blindades s'han de vendre amb bombillo d'alta seguretat però no sempre ocorre això. Una porta blindada que no disposa de bombillo de seguretat podria arribar a obrir-se en qüestió de pocs minuts al no disposar dels mètodes anti robatori que aquests ofereixen.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="faq-item">
							<div class="panel">
								<div class="panel-heading">
									<h4 class="panel-title"><a role="button" data-toggle="collapse" href="#faq6">Em van entrar a robar a casa sense forçar el pany. Tinc clau de seguretat i vaig tancar amb clau. Com és possible?<span class="caret-toggle closed">–</span><span class="caret-toggle opened">+</span></a></h4>
								</div>
								<div id="faq6" class="panel-collapse collapse in">
									<div class="panel-body">
										<p>Probablement en aquest cas lamentablement vostè disposa d'un pany vulnerable als mètodes mes utilitzats per robar en habitatges. Que la seva porta tingui un barret fort amb clau de punts no significa que aquest sigui de seguretat encara que la majoria de la gent profana en la matèria així ho creu. El més segur és que l'intrús hagi utilitzat el mètode Bumping si vostè disposava d'un barret fort en el seu pany o bé per mitjà de ganzúas en cas de pany de gorges (també cridada pany de borjas).</p>
									</div>
								</div>
							</div>
						</div>
						<!-- 
<div class="faq-item">
							<div class="panel">
								<div class="panel-heading">
									<h4 class="panel-title"><a role="button" data-toggle="collapse" href="#faq7">What is a timing belt and how do I know when mine needs replacing?<span class="caret-toggle closed">–</span><span class="caret-toggle opened">+</span></a></h4>
								</div>
								<div id="faq7" class="panel-collapse collapse in">
									<div class="panel-body">
										<p>A timing belt, is a part of an internal combustion engine that controls the timing of the engine’s valves. Your timing belt should be changed every 50,000-70,000 miles, however every manufacturer is different. You should consult your owner’s manual to determine the correct replacement interval.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="faq-item">
							<div class="panel">
								<div class="panel-heading">
									<h4 class="panel-title"><a role="button" data-toggle="collapse" href="#faq8">Does brake fluid really need to be changed?<span class="caret-toggle closed">–</span><span class="caret-toggle opened">+</span></a></h4>
								</div>
								<div id="faq8" class="panel-collapse collapse in">
									<div class="panel-body">
										<p>The average driver uses their brakes 75,000 times per year and takes for granted that they’ll work every time. Today’s brake systems are hydraulic and use brake fluid which is hydrophilic, meaning it can absorb moisture from the air. Once the hydraulic system has moisture in it, corrosion takes place and brake components fail. Brake fluid should be flushed periodically to keep corrosion under control. This procedure is not expensive and is included in many preventative maintenance schedules. Brake fluid should be handled with care. It will melt plastics and remove paint.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="faq-item">
							<div class="panel">
								<div class="panel-heading">
									<h4 class="panel-title"><a role="button" data-toggle="collapse" href="#faq9">How often should antifreeze be replaced?<span class="caret-toggle closed">–</span><span class="caret-toggle opened">+</span></a></h4>
								</div>
								<div id="faq9" class="panel-collapse collapse in">
									<div class="panel-body">
										<p>Antifreeze should be replaced every two years due to oxidation and deterioration of important additives. These additives lubricate the water pump and protect metal parts from rust and oxidation. They also help keep coolant hoses soft and flexible. Today we can recycle and clean old antifreeze, replacing the additives. Antifreeze is very toxic and not environmentally friendly so recycling makes good sense. The engine’s thermostat should also be replaced every 2 years to keep the engine operating temperatures in the proper range.</p>
									</div>
								</div>
							</div>
						</div>
 -->
					</div>
				</div>
			</div>
			<!-- //Panel Block -->
		</div>
		<!-- // Content  -->
<div>[footer]</div>